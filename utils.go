package criuscommander

import "strings"

func ArgsSplitter(in string) []string {
	parts := []string{}

	isInQuote := false
	isEscaping := false
	shouldSkip := false

	start := 0

	for i, c := range in {
		if shouldSkip {
			shouldSkip = false
			continue
		}
		if c == ' ' && !isInQuote {
			parts = append(parts, in[start:i])
			start = i + 1
			continue
		}
		if c == '"' && !isEscaping {
			if isInQuote {
				isInQuote = false
				parts = append(parts, strings.TrimSpace(in[start+1:i]))
				start = i + 2
				shouldSkip = true
				continue
			} else {
				start = i
				isInQuote = true
				continue
			}
		}
		if c == '\\' && !isEscaping {
			isEscaping = true
			continue
		}

		if i == len(in)-1 {
			parts = append(parts, in[start:])
			continue
		}

		isEscaping = false
	}

	return parts
}
