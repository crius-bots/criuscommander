package criuscommander

import "github.com/bwmarrin/discordgo"

type MessageContext struct {
	*discordgo.MessageCreate
	s *discordgo.Session
}

func (c *MessageContext) Send(data interface{}) (*discordgo.Message, error) {
	return c.SendTo(c.ChannelID, data)
}

func (c *MessageContext) SendTo(channelID string, data interface{}) (*discordgo.Message, error) {
	switch d := data.(type) {
	case string:
		return c.s.ChannelMessageSend(channelID, d)
	case *discordgo.MessageSend:
		return c.s.ChannelMessageSendComplex(channelID, d)
	}

	return nil, nil
}

func (c *MessageContext) React(add bool, channelID string, messageID string, emoji string) error {
	if add {
		return c.s.MessageReactionAdd(channelID, messageID, emoji)
	} else {
		return c.s.MessageReactionRemove(channelID, messageID, emoji, "@me")
	}
}
