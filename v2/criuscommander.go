package criuscommander

import (
	"context"
	"errors"
	"github.com/asaskevich/EventBus"
	"github.com/bwmarrin/discordgo"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/sirupsen/logrus"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"io/ioutil"
	"os"
)

type CCConfig struct {
	Platforms       []Platform
	Prefix          string
	AutoloadPlugins bool
	PluginLoaders   []LoaderFactory
	Tokens          map[PlatformType]string
	OtherConfig     map[string]interface{}

	// Where supported, join these channels when commander starts
	AutoJoinChannels map[PlatformType][]interface{}
}

type CommandGroupContainer struct {
	Commands map[string]*Command
	Info     *PluginJSON
}

type Command struct {
	Handler     CommandHandler
	Info        *PJCommand
	Subcommands map[string]*Command
}

type CommandHandler func(*MessageContext, []string) error
type RegisterCommand func(*PJCommand, CommandHandler) error
type GetPlugin func(string) (interface{}, error)

type Commander struct {
	ctx                 context.Context
	commandGroups       map[string]*CommandGroupContainer
	commands            map[string]*Command
	OnError             func(error)
	ccConfig            *CCConfig
	bus                 EventBus.Bus
	registeredPlatforms map[PlatformType]Platform

	// Guards

	// returning false *will stop execution*
	GuardBeforeCommandCheck func(*MessageContext, string, []string) bool
	// returning false *will stop execution*
	GuardBeforeCommandExec func(*MessageContext, string, []string) bool
	GuardPostCommandCheck  func(*MessageContext, string, []string)
}

func (c *Commander) handleError(err error) {
	logrus.Error(err)
}

func (c *Commander) GetCommands() map[string]*Command {
	return c.commands
}

func (c *Commander) GetCommandGroups() map[string]*CommandGroupContainer {
	return c.commandGroups
}

// Deprecated: RegisterCommand does not support subcommands. Do not use it directly.
func (c *Commander) RegisterCommand(groupName string) func(info *PJCommand, handler CommandHandler) error {
	return func(info *PJCommand, handler CommandHandler) error {
		if _, ok := c.commands[info.Activator]; ok {
			return errors.New("a command with this name is already registered")
		}

		command := &Command{
			Handler: handler,
			Info:    info,
		}

		c.commands[info.Activator] = command
		c.commandGroups[groupName].Commands[info.Activator] = command

		return nil
	}
}

// registerCommandChain is the replacement for RegisterCommand that supports subcommands
func (c *Commander) registerCommandChain(group string, info *PJCommand, getHandler GetCommandHandler) (*Command, error) {
	if _, ok := c.commands[info.Activator]; ok {
		return nil, errors.New("a command with this name is already registered")
	}

	handler, err := getHandler(info.HandlerName)
	if err != nil {
		return nil, err
	}

	var subcommands map[string]*Command
	if info.Subcommands != nil && len(info.Subcommands) > 0 {
		subcommands, err = makeSubcommands(info.Subcommands, getHandler, info.Activator)
		if err != nil {
			return nil, err
		}
	}

	command := &Command{
		Handler:     handler,
		Info:        info,
		Subcommands: subcommands,
	}

	c.commands[info.Activator] = command
	c.commandGroups[group].Commands[info.Activator] = command

	return command, nil
}

// Attempt to load a plugin from the disk and set it up using a Loader
func (c *Commander) loadPlugin(name string, loaderFactories map[PluginType]LoaderFactory) error {
	pluginInfo, err := getPluginJSON(name)
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}

	loaderFactory, ok := loaderFactories[pluginInfo.PluginType]
	if !ok {
		logrus.Warnf("Loader %s for plugin %s is not installed.", pluginInfo.PluginType, name)
		return nil
	}

	// get the loader
	loader := loaderFactory.New(context.WithValue(c.ctx, "commander", c))

	c.commandGroups[pluginInfo.Name] = &CommandGroupContainer{
		Commands: make(map[string]*Command),
		Info:     pluginInfo,
	}

	err = loader.LoadPlugin(name)
	if err != nil {
		return err
	}

	for _, command := range pluginInfo.Commands {
		_, err = c.registerCommandChain(pluginInfo.Name, command, loader.GetCommandHandler)
		if err != nil {
			return err
		}
	}

	logrus.Infof("Loaded %s plugin %s", pluginInfo.PluginType, pluginInfo.Name)

	return nil
}

// Deprecated: With the move to loaders, this no longer works properly and you may
// have to faff for it to work. Use the new loader system.
// Register the plugin into the list of 'groups' (read: plugins)
func (c *Commander) SetupPlugin(setup func(RegisterCommand, context.Context), info *PluginJSON) {
	c.commandGroups[info.Name] = &CommandGroupContainer{
		Commands: make(map[string]*Command),
	}

	setup(c.RegisterCommand(info.Name), context.WithValue(c.ctx, "commander", c))
	c.commandGroups[info.Name].Info = info

	logrus.Infof("Loaded plugin %s", info.Name)
}

func (c *Commander) SetupPluginFromConfig(setup func(ctx context.Context) (map[string]CommandHandler, error), info *PluginJSON) error {
	c.commandGroups[info.Name] = &CommandGroupContainer{
		Commands: make(map[string]*Command),
		Info:     info,
	}

	commands, err := setup(context.WithValue(c.ctx, "commander", c))
	if err != nil {
		return err
	}

	for _, command := range info.Commands {
		getHandler := func(name string) (CommandHandler, error) {
			return commands[name], nil
		}

		_, err := c.registerCommandChain(info.Name, command, getHandler)
		if err != nil {
			return err
		}
	}

	logrus.Infof("Loaded plugin %s", info.Name)
	return nil
}

func (c *Commander) HandleError(f func(error)) {
	if f != nil {
		c.OnError = f
		return
	}
	c.OnError = c.handleError
}

func (c *Commander) Open() error {
	for _, platform := range c.ccConfig.Platforms {
		err := platform.Open(context.WithValue(c.ctx, "commander", c))
		if err != nil {
			return err
		}

		// join our channels
		for _, channel := range c.ccConfig.AutoJoinChannels[platform.GetPlatformType()] {
			err := platform.JoinChannel(channel)
			// show the error but don't return because worst case we don't connect to a channel
			if err != nil {
				c.OnError(err)
			}
		}
	}

	return nil
}

// Stop the commander & discord
func (c *Commander) Close() {
	c.bus.Publish(EventCommanderClose)

	for _, platform := range c.ccConfig.Platforms {
		platform.Close()
	}
}

func (c *Commander) autoloadPlugins() error {
	files, err := ioutil.ReadDir("plugins")
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}

	loaderFactories := make(map[PluginType]LoaderFactory)

	for _, loader := range c.ccConfig.PluginLoaders {
		name := loader.GetTypeName()
		loaderFactories[name] = loader
	}

	for _, file := range files {
		if !file.IsDir() {
			continue
		}

		err := c.loadPlugin(file.Name(), loaderFactories)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Commander) JoinChannel(platform PlatformType, channel interface{}) error {
	return c.registeredPlatforms[platform].JoinChannel(channel)
}

func (c *Commander) LeaveChannel(platform PlatformType, channel interface{}) error {
	return c.registeredPlatforms[platform].LeaveChannel(channel)
}

func (c *Commander) SendMessage(platform PlatformType, channelID interface{}, message interface{}) (interface{}, error) {
	client := c.ctx.Value(platform.ToString())

	var (
		data interface{}
	)

	switch platform {
	case PlatformDiscord:
		{
			s := client.(*discordgo.Session)
			switch k := message.(type) {
			case *discordgo.MessageSend:
				return s.ChannelMessageSendComplex(channelID.(string), k)
			case string:
				return s.ChannelMessageSend(channelID.(string), k)
			}
		}
	case PlatformGlimesh:
		{
			s := client.(*goleshchat.Session)
			s.SendMessage(message.(string), channelID.(string))
		}
	case PlatformTwitch:
		{
			s := client.(*twitch.Client)
			s.Say(channelID.(string), message.(string))
		}
	}

	return data, nil
}

// Make a new instance of a commander.
func NewCommander(config *CCConfig, ctx context.Context) (*Commander, error) {
	for _, platform := range config.Platforms {
		t := platform.GetPlatformType()
		err := platform.Setup(config.Tokens[t])
		if err != nil {
			return nil, err
		}

		// load the platform's client into ctx
		ctx = context.WithValue(ctx, platform.GetPlatformType().ToString(), platform.GetClient())
	}

	bus := EventBus.New()

	ctx = context.WithValue(ctx, "bus", bus)
	ctx = context.WithValue(ctx, "prefix", config.Prefix)
	ctx = context.WithValue(ctx, "config", config.OtherConfig)

	cmder := &Commander{
		ctx:                 ctx,
		ccConfig:            config,
		commandGroups:       make(map[string]*CommandGroupContainer),
		commands:            make(map[string]*Command),
		registeredPlatforms: make(map[PlatformType]Platform),
		bus:                 bus,
	}

	for _, platform := range config.Platforms {
		err := platform.RegisterMessageHandler(cmder.onMessage)
		if err != nil {
			return nil, err
		}

		err = platform.RegisterEvents(bus)
		if err != nil {
			return nil, err
		}

		// and finally put the platform into registeredPlatforms
		cmder.registeredPlatforms[platform.GetPlatformType()] = platform
	}

	cmder.OnError = cmder.handleError

	// set up the help command
	command := &Command{
		Handler: cmder.Help,
		Info: &PJCommand{
			Name:               "Help",
			Activator:          "help",
			Help:               "Get some help",
			SupportedPlatforms: PlatformDiscord | PlatformGlimesh | PlatformTwitch,
		},
	}
	cmder.commands["help"] = command

	if config.AutoloadPlugins {
		err := cmder.autoloadPlugins()
		if err != nil {
			return nil, err
		}
	}

	return cmder, nil
}
