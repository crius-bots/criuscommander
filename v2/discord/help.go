package discord

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/crius-bots/criuscommander/v2"
	"sort"
)

func (d *Discord) GenHelp(data interface{}, m *criuscommander.MessageContext) error {
	var (
		embedFields []*discordgo.MessageEmbedField
		embedTitle  string
	)

	prefix := m.Context.Value("prefix").(string)
	config := m.Context.Value("config").(map[string]interface{})

	infoField := &discordgo.MessageEmbedField{
		Name:   "Info",
		Value:  "You can run `" + prefix + "help [Command Group / Command Name]` to get help with a command or group",
		Inline: false,
	}

	embedFields = append(embedFields, infoField)

	switch t := data.(type) {
	case map[string]*criuscommander.CommandGroupContainer:
		{
			embedTitle = "Help"

			// sort the keys into alphabetical just so it looks nicer
			keys := []string{}
			for k := range t {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			// now add all the fields
			for _, k := range keys {
				cg := t[k]

				field := &discordgo.MessageEmbedField{
					Name:   k,
					Value:  fmt.Sprintf("`%s`\n*%d Commands*", cg.Info.Description, len(cg.Commands)),
					Inline: true,
				}

				embedFields = append(embedFields, field)
			}
		}
	case *criuscommander.CommandGroupContainer:
		{
			embedTitle = t.Info.Name + " Help"

			// sort the keys into alphabetical just so it looks nicer
			keys := []string{}
			for k := range t.Commands {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			// now add all the fields
			for _, k := range keys {
				v := t.Commands[k] // grab the element

				field := &discordgo.MessageEmbedField{
					Name:   prefix + k,
					Value:  fmt.Sprintf("`%s`\n*%d Subcommands*", v.Info.Help, len(v.Subcommands)),
					Inline: true,
				}

				embedFields = append(embedFields, field)
			}
		}
	case *criuscommander.Command:
		{
			embedTitle = t.Info.Name + " Help"

			field := &discordgo.MessageEmbedField{
				Name:   prefix + t.Info.Activator,
				Value:  t.Info.Help,
				Inline: false,
			}

			embedFields = append(embedFields, field)

			// subcommands
			if len(t.Subcommands) > 0 {
				// sort the keys into alphabetical just so it looks nicer
				keys := []string{}
				for k := range t.Subcommands {
					keys = append(keys, k)
				}
				sort.Strings(keys)

				// now add all the fields
				for _, k := range keys {
					v := t.Subcommands[k] // grab the element

					field := &discordgo.MessageEmbedField{
						Name:   fmt.Sprintf("Subcommand: %s %s", t.Info.Activator, k),
						Value:  v.Info.Help,
						Inline: true,
					}

					embedFields = append(embedFields, field)
				}
			}
		}
	}

	footerText := d.session.State.User.Username + " help generated with Crius Commander"
	if text, ok := config["HelpEmbedFooterText"]; ok {
		footerText = text.(string)
	}

	authorURL := "https://gitlab.com/crius-bots/criuscommander"
	if url, ok := config["URL"]; ok {
		authorURL = url.(string)
	}

	colour := 16098851
	if c, ok := config["HelpEmbedColour"]; ok {
		colour = c.(int)
	}

	baseEmbed := &discordgo.MessageEmbed{
		Title: embedTitle,
		Color: colour,
		Footer: &discordgo.MessageEmbedFooter{
			Text: footerText,
		},
		Author: &discordgo.MessageEmbedAuthor{
			Name: d.session.State.User.Username,
			URL:  authorURL,
		},
		Fields: embedFields,
	}

	_, err := m.Send(&discordgo.MessageSend{
		Embed: baseEmbed,
	})
	if err != nil {
		return err
	}

	return nil
}
