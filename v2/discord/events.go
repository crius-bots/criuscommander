package discord

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/crius-bots/criuscommander/v2"
)

const (
	EventChannelCreate            = "Commander.Discord.Channel:Create"
	EventChannelDelete            = "Commander.Discord.Channel:Delete"
	EventChannelUpdate            = "Commander.Discord.Channel:Update"
	EventChannelPinsUpdate        = "Commander.Discord.Channel.Pins:Update"
	EventGuildCreate              = "Commander.Discord.Guild:Create"
	EventGuildDelete              = "Commander.Discord.Guild:Delete"
	EventGuildUpdate              = "Commander.Discord.Guild:Update"
	EventGuildBanAdd              = "Commander.Discord.Guild.Ban:Add"
	EventGuildBanRemove           = "Commander.Discord.Guild.Ban:Remove"
	EventGuildEmojiUpdate         = "Commander.Discord.Guild.Emoji:Update"
	EventGuildIntegrationsUpdate  = "Commander.Discord.Guild.Integrations:Update"
	EventGuildMemberAdd           = "Commander.Discord.Guild.Member:Add"
	EventGuildMemberRemove        = "Commander.Discord.Guild.Member:Remove"
	EventGuildMemberUpdate        = "Commander.Discord.Guild.Member:Update"
	EventGuildRoleCreate          = "Commander.Discord.Guild.Role:Create"
	EventGuildRoleDelete          = "Commander.Discord.Guild.Role:Delete"
	EventGuildRoleUpdate          = "Commander.Discord.Guild.Role:Update"
	EventMessageDelete            = "Commander.Discord.Message:Delete"
	EventMessageDeleteBulk        = "Commander.Discord.Message:DeleteBulk"
	EventMessageUpdate            = "Commander.Discord.Message:Update"
	EventMessageReactionAdd       = "Commander.Discord.Message.Reaction:Add"
	EventMessageReactionRemove    = "Commander.Discord.Message.Reaction:Remove"
	EventMessageReactionRemoveAll = "Commander.Discord.Message.Reaction:RemoveAll"
	EventPresenceUpdate           = "Commander.Discord.Presence:Update"
	EventPresencesReplace         = "Commander.Discord.Presences:Replace"
	EventRelationshipAdd          = "Commander.Discord.Relationship:Add"
	EventRelationshipRemove       = "Commander.Discord.Relationship:Remove"
	EventTypingStart              = "Commander.Discord.Typing:Start"
	EventUserGuildSettingsUpdate  = "Commander.Discord.User.Guild.Settings:Update"
	EventUserNoteUpdate           = "Commander.Discord.User.Note:Update"
	EventUserSettingsUpdate       = "Commander.Discord.User.Settings:Update"
	EventUserUpdate               = "Commander.Discord.User:Update"
	EventVoiceServerUpdate        = "Commander.Discord.Voice.Server:Update"
	EventVoiceStateUpdate         = "Commander.Discord.Voice.State:Update"
	EventWebhooksUpdate           = "Commander.Discord.Webhooks:Update"
	// Message.Create intentionally omitted
)

func (d *Discord) RegisterEvents(publisher EventBus.BusPublisher) error {
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.ChannelCreate) {
		publisher.Publish(EventChannelCreate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.ChannelCreate) {
		publisher.Publish(EventChannelDelete, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.ChannelUpdate) {
		publisher.Publish(EventChannelUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.ChannelPinsUpdate) {
		publisher.Publish(EventChannelPinsUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildCreate) {
		publisher.Publish(EventGuildCreate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildDelete) {
		publisher.Publish(EventGuildDelete, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildUpdate) {
		publisher.Publish(EventGuildUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildBanAdd) {
		publisher.Publish(EventGuildBanAdd, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildBanRemove) {
		publisher.Publish(EventGuildBanRemove, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildEmojisUpdate) {
		publisher.Publish(EventGuildEmojiUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildIntegrationsUpdate) {
		publisher.Publish(EventGuildIntegrationsUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildMemberAdd) {
		publisher.Publish(EventGuildMemberAdd, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildMemberRemove) {
		publisher.Publish(EventGuildMemberRemove, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildMemberUpdate) {
		publisher.Publish(EventGuildMemberUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildRoleCreate) {
		publisher.Publish(EventGuildRoleCreate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildRoleDelete) {
		publisher.Publish(EventGuildRoleDelete, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.GuildRoleUpdate) {
		publisher.Publish(EventGuildRoleUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageDelete) {
		publisher.Publish(EventMessageDelete, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageDeleteBulk) {
		publisher.Publish(EventMessageDeleteBulk, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageUpdate) {
		publisher.Publish(EventMessageUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageReactionAdd) {
		publisher.Publish(EventMessageReactionAdd, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageReactionRemove) {
		publisher.Publish(EventMessageReactionRemove, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.MessageReactionRemoveAll) {
		publisher.Publish(EventMessageReactionRemoveAll, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.PresenceUpdate) {
		publisher.Publish(EventPresenceUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.PresencesReplace) {
		publisher.Publish(EventPresencesReplace, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.RelationshipAdd) {
		publisher.Publish(EventRelationshipAdd, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.RelationshipRemove) {
		publisher.Publish(EventRelationshipRemove, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.TypingStart) {
		publisher.Publish(EventTypingStart, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.UserGuildSettingsUpdate) {
		publisher.Publish(EventUserGuildSettingsUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.UserNoteUpdate) {
		publisher.Publish(EventUserNoteUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.UserSettingsUpdate) {
		publisher.Publish(EventUserSettingsUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.UserUpdate) {
		publisher.Publish(EventUserUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.VoiceServerUpdate) {
		publisher.Publish(EventVoiceServerUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.VoiceStateUpdate) {
		publisher.Publish(EventVoiceStateUpdate, e)
	})
	d.session.AddHandler(func(s *discordgo.Session, e *discordgo.WebhooksUpdate) {
		publisher.Publish(EventWebhooksUpdate, e)
	})

	return nil
}

func (d *Discord) RegisterMessageHandler(fn func(string, *criuscommander.MessageContext)) error {
	d.session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		ctx := context.WithValue(d.ctx, "platform", d.GetPlatformType())
		mc := &criuscommander.MessageContext{
			PlatformData: m,
			Context:      ctx,
			Message:      *makeMessage(m),
		}

		fn(m.Content, mc)
	})

	return nil
}

func makeMessage(m *discordgo.MessageCreate) *criuscommander.Message {
	return &criuscommander.Message{
		RealmID: m.GuildID,
		Author: criuscommander.MessageUser{
			ID:         m.Author.ID,
			Username:   m.Author.Username,
			IsMod:      func() bool { return false }, // you'll have to do this yourself, soz.
			IsStreamer: func() bool { return false }, // what's a ~computer~ streamer?
		},
	}
}
