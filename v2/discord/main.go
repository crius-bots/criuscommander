package discord

import (
	"context"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/crius-bots/criuscommander/v2"
)

func New() criuscommander.Platform {
	return &Discord{}
}

type Discord struct {
	session *discordgo.Session
	ctx     context.Context
}

func (d *Discord) Setup(token string) error {
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		return err
	}

	d.session = session
	return nil
}

func (d *Discord) GetPlatformType() criuscommander.PlatformType {
	return criuscommander.PlatformDiscord
}

func (d *Discord) GetClient() interface{} {
	return d.session
}

func (d *Discord) Open(ctx context.Context) error {
	d.ctx = ctx
	return d.session.Open()
}

func (d *Discord) Close() error {
	return d.session.Close()
}

// isn't supported under discord.
func (d *Discord) JoinChannel(_ interface{}) error {
	return nil
}

// isn't supported under discord.
func (d *Discord) LeaveChannel(_ interface{}) error {
	return nil
}
