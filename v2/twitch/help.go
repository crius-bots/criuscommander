package twitch

import "gitlab.com/crius-bots/criuscommander/v2"

func (t *Twitch) GenHelp(_ interface{}, m *criuscommander.MessageContext) error {
	m.Send("Crius help is available at https://crius.rpgpn.co.uk/docs/commands/")
	return nil
}
