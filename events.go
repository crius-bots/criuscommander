package criuscommander

import (
	"github.com/bwmarrin/discordgo"
	"strings"
)

func (c *Commander) reactionAdd(s *discordgo.Session, r *discordgo.MessageReactionAdd) {
	for _, v := range c.commandGroups {
		err := v.cg.OnReaction(s, r)
		if err != nil {
			c.OnError(err)
		}
	}
}

func (c *Commander) reactionRemove(s *discordgo.Session, r *discordgo.MessageReactionRemove) {
	for _, v := range c.commandGroups {
		err := v.cg.OnReaction(s, r)
		if err != nil {
			c.OnError(err)
		}
	}
}

func (c *Commander) messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	// this should be a command
	if strings.HasPrefix(m.Content, c.ccConfig.Prefix) {
		args := ArgsSplitter(m.Content[1:])
		if command, ok := c.commands[args[0]]; ok {
			ctx := &MessageContext{m, s}

			err := command.Handler(ctx, args[1:])
			if err != nil {
				c.OnError(err)
			}
		}
	}
}

func (c *Commander) guildJoin(s *discordgo.Session, event *discordgo.GuildCreate) {
	if event.Guild.Unavailable {
		return
	}

	for _, v := range c.commandGroups {
		err := v.cg.OnGuildJoin(s, event)
		if err != nil {
			c.OnError(err)
		}
	}
}

func (c *Commander) guildLeave(s *discordgo.Session, event *discordgo.GuildDelete) {
	for _, v := range c.commandGroups {
		err := v.cg.OnGuildLeave(s, event)
		if err != nil {
			c.OnError(err)
		}
	}
}
